# FINTECH 512 PSP Defect Report

# Description - describe the problem, what you expected, and what happened instead.

# Defect Type

- [] Documentation: Incorrect or obsolete comments or information messages
- [] Syntax: Program syntax is either invalid or semantically incorrect because of spelling, punctuation, typos, instruction formats, misplaced parentheses, and so forth
- [] Build, Package: Build problems external to the source code or compiled components including change management, version control, library, make files
- [] Assignment: Wrong or missing declaration or initialization, duplicate names, violations, or failure to set scope or limits
- [] Interface: Wrong or missing procedure calls, references, incorrect user or file I/O processing, wrong user formats, interface mismatches, missing ‘includes’
- [] Checking: Wrong or missing error messages or warnings, failure to sanitize input,  inadequate checks
- [] Data: Wrong data structure or content, missing values
- [] Function: Does something wrong manipulating data or control, for example, logic, pointers, loops, recursion, computation, function defects
- [] System: Problem with the platform, other applications or services such as configuration, timing, memory
- [] Environment: Development environment support system problems such as IDE, design system, compiler version, test harness or cases

# Defect introduced during:
- [] Planning
- [] Development
- [] Testing
- [] Other (describe)

# Defect removed during:
- [] Planning
- [] Development
- [] Testing
- [] Other (describe)